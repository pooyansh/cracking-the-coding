import numpy as np


def search_for_identity(list_1):
    min = 0
    max = len(list_1) -1
    index = -1
    while max - min > 1:
        middle = min + (max -min)//2
        if (middle+1) == list_1[middle]:
            return middle
        elif middle > list_1[middle]:
            max = middle
        else:
            min = middle

    return index

def find_the_largest_element(list_1, x):
    index = -1
    list_1.sort()
    if list_1[-1] < x:
        return index
    elif list_1[0] > x:
        return 0
    min = 0
    max = len(list_1) - 1

    while max - min > 1:
        middle = min + (max - min)//2
        if list_1[middle] > x:
            max = middle
        else:
            min = middle
    return max


def find_index_hold_me(list_1, x):
    index = -1
    if list_1[0] > x or list_1[-1] < x:
        return index
    list_1.sort()
    min = 0
    max = len(list_1) - 1

    while max - min > 1:
        middle = min + (max - min)//2
        if list_1[middle] > x:
            max = middle
        elif list_1[middle] < x:
            min = middle
        else:
            index = middle
            return index

    return index


def square_root_int(n):
    max = n
    min = 1
    while max - min >= 2:
        middle = min + (max - min)//2
        if middle*middle > n:
            max = middle
        elif middle*middle < n:
            min = middle
        else:
            return  middle
    if max*max <= n:
        return max
    else:
        return min


def test_square_root_int(x):
    root_i = square_root_int(x)
    root_t = np.floor(np.sqrt(x))
    if np.abs(root_i - root_t) > 1e-8:
       yield 'failed'


if __name__ == '__main__':
   """ np.random.seed(64)
    a = np.random.randint(0,50,size=40 )
    a.sort()
    n = np.random.randint(0,50)
    a.sort()
    x = find_index_hold_me(a, n)
    if x != -1:
        print(n, x, '\n', a[x],a)
    else:
        print(n)
        print(a)
        print(a[np.where(a ==n)])
    """
   np.random.seed(64)
   a = [0, 1,2,3,5,7,8]
   a.sort()

   index = search_for_identity(a)
   print(a)
   print(index)
   print(a[index])

