import numpy as np
import os
import sys


def bubble_sort(unsorted_list):
    """

    :param unsorted_list:
    :return:
    """
    len_list = len(unsorted_list)
    if len_list > 1:
        while True:
            idx = 0
            switch_count = 0
            while idx < len_list-1:
                if unsorted_list[idx] > unsorted_list[idx+1]:
                    temp = unsorted_list[idx]
                    unsorted_list[idx] = unsorted_list[idx+1]
                    unsorted_list[idx+1] = temp
                    switch_count += 1
                idx += 1
            if switch_count == 0:
                break

    return unsorted_list


def quick_sort(unsorted_list, low=0, high=None):
    """
    :param unsorted_list:
    :param low:
    :param high:
    :return:
    """

    def partition(unsorted_list, idx_low, idx_high):
        i = idx_low - 1
        pivot = unsorted_list[idx_high]
        for j in range(idx_low, idx_high):
            if unsorted_list[j] < pivot:
                i = i + 1
                temp = unsorted_list[j]
                unsorted_list[j] = unsorted_list[i]
                unsorted_list[i] = temp
        temp = unsorted_list[i+1]
        unsorted_list[i+1] = unsorted_list[idx_high]
        unsorted_list[idx_high] = temp
        return i+1



    if high is None:
        high = len(unsorted_list) - 1
    if low < high:
        pi = partition(unsorted_list, low, high)
        quick_sort(unsorted_list, low, pi-1)
        quick_sort(unsorted_list, pi+1, high)

    return unsorted_list


def merge_sort(unsorted_list):
    """

    :param unsorted_list:
    :param l_idx:
    :param u_idx:
    :return:
    """
    if len(unsorted_list) > 1 :
        mid_idx = len(unsorted_list)//2
        L = unsorted_list[:mid_idx]
        R = unsorted_list[mid_idx:]
        merge_sort(L)
        merge_sort(R)
        unsorted_list.clear()

        while len(L) > 0 and len(R)>0:
            if L[0] > R[0]:
                unsorted_list.append(R[0])
                R.pop(0)
            else:
                unsorted_list.append(L[0])
                L.pop(0)

        for item in R:
            unsorted_list.append(item)
        for item in L:
            unsorted_list.append(item)


def radix_sort(unsorted_list):
    """

    :param unsorted_list:
    :return:
    """
    def counting_sort(unsorted_list, exp1):
        n = len(unsorted_list)
        output = [0] * n
        count = [0] * 10
        for i in range(n):
            index =np.int((unsorted_list[i])/exp1)
            count[(index)%10] += 1

        for i in range(1, 10):
            count[i] += count[i-1]

        i = n-1
        while i >= 0:
            index = np.int(unsorted_list[i]/exp1)
            idx = (index)%10
            output[count[idx] -1] = unsorted_list[i]
            count[idx] -= 1
            i -= 1

        i = 0
        for i in range(n):
            unsorted_list[i] = output[i]


    max = np.int(np.max(unsorted_list))
    exp = 1
    while max/exp > 0:
        counting_sort(unsorted_list, exp)
        exp = 10*exp
    return unsorted_list


if __name__ == '__main__':
    list_1 = [10, 5, 1, 4, 2, 3, 11]
    list_1 = [ 170, 45, 75, 90, 802, 24, 2, 66 ]
    list_1_sorted = radix_sort(list_1.copy())
    print(list_1)
    print(list_1_sorted)