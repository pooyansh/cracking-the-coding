import os
import sys
import numpy as np


def error_line():
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_fname.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno)


class NodeBinaryTree:
    def __init__(self, data=None):
        self.data = data
        self.lnode = None
        self.rnode = None


class BinaryTree:
    """
    Fast implementation of binary tree
    """
    def __init__(self, nodes=None):
        self.root = None
        if nodes is not None:
            nodes.sort()
            self.root = BinaryTree.insert_sorted_list(self.root, nodes)

    @staticmethod
    def insert_sorted_list(node, list_nodes):
        try:
            max_idx = len(list_nodes)
            if max_idx == 0:
                node = None
            elif max_idx == 1:
                node = NodeBinaryTree(list_nodes[0])
            else:
                mid_index = 0 + max_idx//2
                node = NodeBinaryTree(list_nodes[mid_index])
                node.lnode = BinaryTree.insert_sorted_list(node.lnode, list_nodes[:mid_index])
                if mid_index + 1 < max_idx:
                    node.rnode = BinaryTree.insert_sorted_list(node.rnode, list_nodes[mid_index+1:])
                else:
                    node.rnode = None
            return node
        except RuntimeError as err:
            error_line()
            print(err)


    @staticmethod
    def print_node(node):
        if node.data is None:
            return 'None'
        if node.lnode is None:
            str_left = 'None'
        else:
            str_left = BinaryTree.print_node(node.lnode)

        if node.rnode is None:
            str_right = 'None'
        else:
            str_right = BinaryTree.print_node(node.rnode)

        str_mid = node.data

        str_result = "\t\u2190 {mid} \u2192\n{left}\t     \t{right}".format(mid=str_mid, left=str_left, right=str_right)
        #return str_left, str_mid, str_right
        return str_result

    @staticmethod
    def insert_node(node, data):
        """

        :param node:
        :param data:
        :return:
        """
        if node.data > data:
            if node.lnode is None:
                node.lnode = NodeBinaryTree(data)
                return 0
            else:
                BinaryTree.insert_node(node.lnode, data)
        elif node.data < data:
            if node.rnode is None:
                node.rnode = NodeBinaryTree(data)
                return 0
            else:
                BinaryTree.insert_node(node.rnode, data)
        else:
            return 0

    @staticmethod
    def travers_tree(node):
        result = []
        if node is not None:
            l_list = []
            r_list = []
            if node.lnode is not None:
                l_list = BinaryTree.travers_tree(node.lnode)
            if node.rnode is not None:
                r_list = BinaryTree.travers_tree(node.rnode)
            result = l_list + [node.data] + r_list

        return result




    def insert(self, data):
        """
        :param data:
        :return:
        """
        if self.root is None:
            self.root = NodeBinaryTree(data)
        else:
            if self.root.data > data:
                BinaryTree.insert_node(self.root.lnode, data)
            elif self.root.data < data:
                BinaryTree.insert_node(self.root.rnode, data)
            else:
                return 0

    def __repr__(self):
        if self.root is None:
            return 'None'
        else:
            # str_left, str_mid, str_right = BinaryTree.print_node(self.head)
            str_left = BinaryTree.print_node(self.root)
        return "{0}".format(str_left) # , str_right, str_right)

    def floor(self,data):
        # TODO complete floor
        pass

    def number_subtree(self):
        # TODO complete number subtree
        pass

    def to_order_list(self):
        if self.root is None:
            return None
        else:
            travers_list = BinaryTree.travers_tree(self.root)
            return travers_list


if __name__ == '__main__':
    binary_tree = BinaryTree([10, 11, 12, 14, 15, 20, 21, 25, 29])
    list_1 = binary_tree.to_order_list()
    list_1
