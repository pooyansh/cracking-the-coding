"""
author : pooyan.shirvani@gmail.com
"""
import os
import sys


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return str(self.data)

    def __del__(self):
        del self.data


class LinkedList:
    def __init__(self, nodes=None):
        self.head = None
        if nodes is not None:
            node = Node(nodes.pop(0))
            self.head = node
            for item in nodes:
                node.next = Node(item)
                node = node.next

    def __repr__(self):
        node = self.head
        nodes = []
        while node is not None:
            nodes.append(node.data)
            node = node.next
        nodes.append("None")
        return "->".join(nodes)

    def __iter__(self):
        node = self.head
        while node is not None:
            yield node
            node = node.next

    def append(self, new_data):
        try:
            if self.head is not None:
                node = self.head
                while node.next is not None:
                    node = node.next
                new_node = Node(new_data)
                node.next = new_node
            else:
                self.head = Node(new_data)
        except IOError as err:
            print(err)
        except RuntimeError as err:
            print(err)

    def appendleft(self, new_data):
        node = self.head
        new_node = Node(new_data)
        new_node.next = node
        self.head = new_node

    def pop(self):
        node = self.head
        while node.next is not None:
            if node.next.next is None:
                del node.next
                node.next = None
            else:
                node = node.next

    def popleft(self):
        node = self.head
        self.head = node.next
        del node

    def __getitem__(self, item):
        try:
            if self.head is None:
                raise RuntimeError('Empty list')
            if item >= 0:
                i = 0
                node = self.head
                while i != item:
                    if node.next is None:
                        raise IndexError("Index out of range")
                    else:
                        node = node.next
                        i += 1
                return node
            else:
                raise TypeError("Only support positive indexing")

        except RuntimeError as err:
            print(err)



if __name__ == '__main__':
    llist = LinkedList(['a', 'b', 's'])
    llist.append('a')
    print(llist)
    print(llist[1])
    print(llist[3])

